//
// Created by malyp on 11/5/2022.
//

#include <utility>
#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
#include "../LoadDataService.h"
#include "../FinderService.h"
#include "../TemperatureCalculatorService.h"


std::vector<std::vector<std::string> > LoadDataService::parseCsv(std::string csvFileLocation) {
    std::cout<<"Parsing CSV: "<<csvFileLocation<<",  --- start"<<std::endl;

    std::ifstream data(csvFileLocation);
    std::string line;
    std::vector<std::vector<std::string> > parsedCsv;
    while(std::getline(data,line)){
        std::stringstream lineStream(line);
        std::string cell;
        std::vector<std::string> parsedRow;
        while(std::getline(lineStream,cell,';')){
            parsedRow.push_back(cell);
        }

        parsedCsv.push_back(parsedRow);
    }
    if(parsedCsv.empty()){
        std::cout<<"CSV: "<<csvFileLocation<<"; empty or couldn't load"<<std::endl;
    }
    std::cout<<"Parsing CSV: "<<csvFileLocation<<",  --- end"<<std::endl;

    return parsedCsv;
}

InterestType LoadDataService::parseInterestType(std::string interestTypeString) {
    if (interestTypeString == "OC") {
        return SHOPPING_CENTER;
    }else if (interestTypeString == "AM") {
        return ADMINISTRATION_BUILDING;
    }else if (interestTypeString == "AC") {
        return ADMINISTRATION_CENTER;
    }else if (interestTypeString == "KU") {
        return CULTURE;
    }else if (interestTypeString == "ANS") {
        return BUILD_CHARGING_STATION;
    }else if (interestTypeString == "ZPID") {
        return PID;
    }else if (interestTypeString == "CPN") {
        return CAR_PARKING;
    }else {
        std::cout<<"Unknown point of interest type: " << interestTypeString<<std::endl;
        throw std::invalid_argument( "Unknown point of interest type: " + interestTypeString );
    }
}

PragueMap LoadDataService::generatePointOfInterestMap(std::string csvFileLocation) {
    auto parsedCsvData = parseCsv(std::move(csvFileLocation));
    PragueMap pragueMap;
    std::cout<<"Converting string to numbers --- start"<<std::endl;
    for(auto data: parsedCsvData ){
        if(parsedCsvData[0][0] == data[0]){
            continue;
        }
        std::replace( data[1].begin(), data[1].end(), ',', '.');
        std::replace( data[2].begin(), data[2].end(), ',', '.');
        auto id = std::stoul(data[0]);
        auto coordinateX = std::stold(data[1]);
        auto coordinateY = std::stold(data[2]);
        InterestType interestType = parseInterestType(data[3]);
        unsigned long noise=0L;
        if(interestType==CAR_PARKING){
            noise = std::stoul(data[4]);
        }
        PointOfInterest newPointOfInterest = PointOfInterest(coordinateX, coordinateY, id,noise, interestType);
        pragueMap.addPointOfInterest(newPointOfInterest);
    }

    std::cout<<"Converting string to numbers --- end"<<std::endl;


    return pragueMap;
}

void LoadDataService::loadChargingStations(PragueMap &pragueMap, const std::string &csvFileLocation) {
    auto parsedCsvData = parseCsv(csvFileLocation);
    for(auto data: parsedCsvData ){
        if(parsedCsvData[0][0] == data[0]){
            continue;
        }
        std::replace( data[1].begin(), data[1].end(), ',', '.');
        std::replace( data[2].begin(), data[2].end(), ',', '.');
        auto id = std::stoul(data[0]);
        auto coordinateX = std::stold(data[1]);
        auto coordinateY = std::stold(data[2]);

        PossibleChargingStation newPossibleChargingStation = PossibleChargingStation(coordinateX, coordinateY, id);
        pragueMap.addPossibleChargingPoint(newPossibleChargingStation);
    }
    std::cout<<"Finding optimal neighbours --- start"<<std::endl;
    auto interestsToFind = FinderService::getInterestTypesToFind();
    int i=0;
    for (auto &possibleChargingStationRow : pragueMap.getPossibleChargingStations()){
        for(auto & possibleChargingStationPoint : possibleChargingStationRow.second){
            i++;
            if(i%100==0){
                std::cout<<"Finding optimal neighbours: "<<i<<" out of: "<<pragueMap.getPossibleChargingStations().size()<<" --- start"<<std::endl;

            }
            for(auto interestToFind : interestsToFind){
                auto interestingPoints = FinderService::getInterestPointsByType(pragueMap, possibleChargingStationPoint.second, interestToFind);
                possibleChargingStationPoint.second.addPointOfInterests(interestingPoints);
            }
        }
    }
    std::cout<<"Finding optimal neighbours --- start"<<std::endl;

}



void LoadDataService::loadCloseByNeighbours(PragueMap &pragueMap) {
    std::cout<<"Finding close charging neighbours --- start"<<std::endl;
    int i=0;
    for (auto &possibleChargingStationRow : pragueMap.getPossibleChargingStations()){
        for(auto & possibleChargingStationPoint : possibleChargingStationRow.second){
            i++;
            if(i%100==0){
                std::cout<<"Finding close charging neighbours: "<<i<<" out of: "<<pragueMap.getPossibleChargingStations().size()<<" --- start"<<std::endl;

            }
            auto neighbours = FinderService::getAllCloseByChargingStations(pragueMap,
                                                                           possibleChargingStationPoint.second,
                                                                           200);
            possibleChargingStationPoint.second.addNeighbours(neighbours);
        }
    }
    std::cout<<"Finding close charging neighbours --- end"<<std::endl;


}

void LoadDataService::loadBaseTemperatures(PragueMap &pragueMap) {
    std::cout<<"Loading temperature --- start"<<std::endl;
    int i=0;
    for (auto &possibleChargingStationRow : pragueMap.getPossibleChargingStations()){
        for(auto & possibleChargingStationPoint : possibleChargingStationRow.second){
            i++;
            if(i%100==0){
                std::cout<<"Loading temperature: "<<i<<" out of: "<<pragueMap.getPossibleChargingStations().size()<<" --- continue"<<std::endl;

            }
            int baseHeat = TemperatureCalculatorService::calculateHeat(possibleChargingStationPoint.second,possibleChargingStationPoint.second.getObjectsUsedInBaseTemperature());
            possibleChargingStationPoint.second.setBaseTemperature(baseHeat);
        }
    }
    std::cout<<"Loading temperature --- end"<<std::endl;
}

void LoadDataService::removeAbsoluteZeroPotentialChargingStations(PragueMap &pragueMap) {
    std::cout<<"Remove absolute zero possible charging stations --- start"<<std::endl;
    std::list<PossibleChargingStation> chargingPointsToDelete;
    for (auto &possibleChargingStationRow : pragueMap.getPossibleChargingStations()){
        for(const auto& possibleChargingStationPoint : possibleChargingStationRow.second){
            if(possibleChargingStationPoint.second.getBaseTemperature()<=0){
                chargingPointsToDelete.push_back(possibleChargingStationPoint.second);
            }
        }
    }
    for(const auto& chargingPointToDelete : chargingPointsToDelete){
        auto x = pragueMap.getPossibleChargingStations().find(chargingPointToDelete.getCoordinateX());
        x->second.erase(chargingPointToDelete.getCoordinateY());
        if(x->second.empty()){
            pragueMap.getPossibleChargingStations().erase(x);
        }
    }

    std::cout<<"Remove absolute zero possible charging stations --- end"<<std::endl;

}


