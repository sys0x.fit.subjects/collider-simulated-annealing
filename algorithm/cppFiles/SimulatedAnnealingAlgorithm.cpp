//
// Created by malyp on 11/6/2022.
//

#include <iostream>
#include "../SimulatedAnnealingAlgorithm.h"
#include "../../services/SimulatedAnnealingService.h"
#include "../../services/MapWriterService.h"

using namespace std;
void SimulatedAnnealingAlgorithm::anneal(PragueMap &pragueMap) {
    vector<PossibleChargingStation *> pickedChargingStations;
    pickStartingChargingStations(pragueMap,pickedChargingStations);
    set<PossibleChargingStation *> pickedChargingStationsSet(pickedChargingStations.begin(),pickedChargingStations.end());

    int heat=1000;
    srand(time (NULL));
    while(heat>=0) {
        for (int i = 0; i < 1000; i++) {
            size_t randomStation = (rand() % pickedChargingStations.size());
            auto pickedChargingStation = pickedChargingStations.at(randomStation);
            auto neighbours = SimulatedAnnealingService::getNeighbours(pragueMap, *pickedChargingStation, heat);
            if(neighbours.empty()){
                continue;
            }
            size_t randomNeighbour = (rand() % neighbours.size());
            auto pickedNeighbour = neighbours.at(randomNeighbour);
            if(pickedChargingStationsSet.contains(pickedNeighbour)){
                continue;
            }
            auto shouldISwitch = SimulatedAnnealingService::shouldISwitch(*pickedChargingStation, *pickedNeighbour,
                                                                          heat);
            if (shouldISwitch) {
                pickedChargingStationsSet.erase(pickedChargingStation);
                pickedChargingStationsSet.insert(pickedNeighbour);

                SimulatedAnnealingService::switchChargingStations(*pickedChargingStation, *pickedNeighbour);
                pickedChargingStations.erase(pickedChargingStations.begin()+randomStation);
                pickedChargingStations.push_back(pickedNeighbour);
            }

        }
        cout<<"Temperature:"<<heat<<endl;
        heat--;
        MapWriterService::writeHeatMap(pragueMap,"../data/OngoingHeatMap-50-1000-1000-1-"+to_string(heat)+".csv");
        MapWriterService::writeChosenChargingStations(pickedChargingStations,"../data/OngoingHeatMapPoint-50-1000-1000-1-"+to_string(heat)+".csv");
    }
    MapWriterService::writeHeatMap(pragueMap,"../data/EndHeatMap-50-1000-1000-1.csv");
    MapWriterService::writeChosenChargingStations(pickedChargingStations,"../data/ChosenChargingStationsPoint-50-1000-1000-1.csv");
}

std::vector<PossibleChargingStation *> SimulatedAnnealingAlgorithm::pickStartingChargingStations(PragueMap &pragueMap, vector<PossibleChargingStation *> &pickedChargingStations) {

    int i=0;
    for (auto &possibleChargingStationRow : pragueMap.getPossibleChargingStations()){
        for(auto &possibleChargingStationPoint : possibleChargingStationRow.second){
            if(i==500){
                return pickedChargingStations;
            }
            pickedChargingStations.push_back(&possibleChargingStationPoint.second);
            SimulatedAnnealingService::addChargingStation(possibleChargingStationPoint.second);
            i++;
        }
    }
    return pickedChargingStations;
}
