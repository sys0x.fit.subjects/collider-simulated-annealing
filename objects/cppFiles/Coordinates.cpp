//
// Created by malyp on 11/5/2022.
//

#include "../Coordinates.h"

Coordinates::Coordinates(long double coordinateX, long double coordinateY) : coordinateX(coordinateX), coordinateY(coordinateY) {}

long double Coordinates::getCoordinateX() const {
    return coordinateX;
}

long double Coordinates::getCoordinateY() const {
    return coordinateY;
}

void Coordinates::setCoordinateX(long double coordinateX) {
    Coordinates::coordinateX = coordinateX;
}

void Coordinates::setCoordinateY(long double coordinateY) {
    Coordinates::coordinateY = coordinateY;
}
std::ostream &operator<<(std::ostream &os, const Coordinates &coordinates) {
    os << "coordinateX: " << coordinates.coordinateX << " coordinateY: " << coordinates.coordinateY;
    return os;
}

