//
// Created by malyp on 11/5/2022.
//

#ifndef SIMULATED_ANNEALING_CHARGING_STATIONS_POSSIBLECHARGINGSTATION_H
#define SIMULATED_ANNEALING_CHARGING_STATIONS_POSSIBLECHARGINGSTATION_H


#include "PointOfInterest.h"
#include <list>
#include <ostream>
#include <set>
#include <map>

class PossibleChargingStation : public Coordinates {
private:
    unsigned long id;
    long baseTemperature=-1;
    long coolingTemperature=0;
    std::map<PossibleChargingStation *, long> coolingFactor;
    std::list<PossibleChargingStation *> closeByChargingStations;
    std::list<const PointOfInterest *> objectsUsedInBaseTemperature;

public:
    PossibleChargingStation(long double coordinateX, long double coordinateY, unsigned long id);
    PossibleChargingStation(const PossibleChargingStation & possibleChargingStation);
    void addPointOfInterests(const std::list<const PointOfInterest *> & pointOfInterests);
    void addNeighbours(const std::list<PossibleChargingStation *> & neighbours);
    void addNearByChargingStation(PossibleChargingStation * chargingStationToAdd);
    void deleteNearByChargingStation(PossibleChargingStation * chargingStationToDelete);

    long getCoolingTemperature(PossibleChargingStation * neighbourChargingStation) const;
    long getPossibleCoolingTemperature(PossibleChargingStation * possibleNeighbourChargingStation) const;

    std::list<PossibleChargingStation *> &getCloseByChargingStations();

    long getBaseTemperature() const;

    long getCoolingTemperature1() const;

    void setBaseTemperature(long baseTemperature);

    const std::list<const PointOfInterest *> &getObjectsUsedInBaseTemperature() const;

    unsigned long getId() const;
    friend std::ostream &operator<<(std::ostream &os, const PossibleChargingStation &station);
};


#endif //SIMULATED_ANNEALING_CHARGING_STATIONS_POSSIBLECHARGINGSTATION_H
