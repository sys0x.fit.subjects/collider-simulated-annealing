//
// Created by malyp on 11/5/2022.
//

#ifndef SIMULATED_ANNEALING_CHARGING_STATIONS_FINDERSERVICE_H
#define SIMULATED_ANNEALING_CHARGING_STATIONS_FINDERSERVICE_H

#include <list>
#include "../objects/PointOfInterest.h"
#include "../objects/PragueMap.h"

class FinderService {
private:
public:
    static long getWalkingDistance(InterestType interestType);
    static unsigned long calculateHypotenuse(const Coordinates &one,const Coordinates &two);
    static std::list<const PointOfInterest *> getInterestPointsByType(const PragueMap &pragueMap, const Coordinates & searchArea, const InterestType &interestType);
    static std::list<PossibleChargingStation *>
    getAllCloseByChargingStations(PragueMap &pragueMap, const Coordinates &searchArea,
                                  const int &walkingDistance,bool useCircle = true);
    static std::list<InterestType> getInterestTypesToFind();

};


#endif //SIMULATED_ANNEALING_CHARGING_STATIONS_FINDERSERVICE_H
