//
// Created by malyp on 11/5/2022.
//

#include <stdexcept>
#include <iostream>
#include "../PointOfInterest.h"


PointOfInterest::PointOfInterest(long double coordinateX, long double coordinateY, unsigned long id,unsigned long noise, InterestType interestType) :
                                                                Coordinates(coordinateX, coordinateY), id(id),
                                                                noise(noise), interestType(interestType) {}

PointOfInterest::PointOfInterest(const PointOfInterest &pointOfInterest) : Coordinates(pointOfInterest.getCoordinateX(),pointOfInterest.getCoordinateY()),
                                                                           interestType(pointOfInterest.interestType),
                                                                           id(pointOfInterest.id),
                                                                           noise(pointOfInterest.noise) {}

std::ostream &operator<<(std::ostream &os, const PointOfInterest &interest) {
    os << "id: " << interest.id << " interestType: "<< interest.interestType;
    return os;
}

InterestType PointOfInterest::getInterestType() const {
    return interestType;
}

