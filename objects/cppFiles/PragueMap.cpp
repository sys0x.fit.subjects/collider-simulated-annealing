//
// Created by malyp on 11/5/2022.
//

#include <stdexcept>
#include <iostream>
#include "../PragueMap.h"

void PragueMap::addPointOfInterest(PointOfInterest pointOfInterest) {
    auto mapX = mapWithPointOfInterests.find(pointOfInterest.getCoordinateX());

    if(mapX == mapWithPointOfInterests.end()){
        mapWithPointOfInterests.insert(std::make_pair(pointOfInterest.getCoordinateX(), std::map<long double,PointOfInterest>()));
        mapX = mapWithPointOfInterests.find(pointOfInterest.getCoordinateX());
    }
    auto mapY = mapX->second.find(pointOfInterest.getCoordinateY());
    if(mapY != mapX->second.end()){
        std::cout<<"Point in that area already exists: "<<pointOfInterest<<"; moving a bit up"<<std::endl;
        pointOfInterest.setCoordinateY(pointOfInterest.getCoordinateY()+0.000000001L);
        return addPointOfInterest(pointOfInterest);
    }
    mapX->second.insert(std::make_pair(pointOfInterest.getCoordinateY(),pointOfInterest));
}

void PragueMap::addPossibleChargingPoint( PossibleChargingStation possibleChargingStation) {
    auto mapX = mapOfPossibleChargingStations.find(possibleChargingStation.getCoordinateX());

    if(mapX == mapOfPossibleChargingStations.end()){
        mapOfPossibleChargingStations.insert(std::make_pair(possibleChargingStation.getCoordinateX(), std::map<long double,PossibleChargingStation>()));
        mapX = mapOfPossibleChargingStations.find(possibleChargingStation.getCoordinateX());
    }
    auto mapY = mapX->second.find(possibleChargingStation.getCoordinateY());
    if(mapY != mapX->second.end()){
        std::cout<<"Point in that area already exists: "<<possibleChargingStation<<"; moving a bit up"<<std::endl;
        possibleChargingStation.setCoordinateY(possibleChargingStation.getCoordinateY()+0.000000001L);
        return addPossibleChargingPoint(possibleChargingStation);
    }
    mapX->second.insert(std::make_pair(possibleChargingStation.getCoordinateY(),possibleChargingStation));
}

const std::map<long double, std::map<long double, PointOfInterest>> &PragueMap::getMapWithPointOfInterests() const {
    return mapWithPointOfInterests;
}

const std::map<long double, std::map<long double, PossibleChargingStation>> &
PragueMap::getMapOfPossibleChargingStations() const {
    return mapOfPossibleChargingStations;
}

std::map<long double,std::map<long double,PossibleChargingStation>> &PragueMap::getPossibleChargingStations(){
    return mapOfPossibleChargingStations;
}

