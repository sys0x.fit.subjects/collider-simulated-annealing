//
// Created by malyp on 11/5/2022.
//

#ifndef SIMULATED_ANNEALING_CHARGING_STATIONS_SIMULATEDANNEALINGSERVICE_H
#define SIMULATED_ANNEALING_CHARGING_STATIONS_SIMULATEDANNEALINGSERVICE_H


#include <list>
#include "../objects/PossibleChargingStation.h"
#include "../objects/PragueMap.h"

class SimulatedAnnealingService {
private:
    static int calculateNewTemperatureDelta(PossibleChargingStation &possibleChargingStation);
    static int calculateCurrentTemperatureDelta(PossibleChargingStation &possibleChargingStation);
    static int calculateSwitchTemperatureDelta(PossibleChargingStation &currentChargingStation,PossibleChargingStation &proposedChargingStation) ;
public:
    static std::vector<PossibleChargingStation*> getNeighbours(PragueMap & pragueMap, const Coordinates &startingCoordinates, const int & currentTemperature);
    static bool shouldISwitch( PossibleChargingStation & currentChargingStation, PossibleChargingStation &possibleChargingStation, const int & currentTemperature) ;
    static void switchChargingStations(PossibleChargingStation &oldChargingStation, PossibleChargingStation &newStation);

    static void addChargingStation(PossibleChargingStation &newStation);
};


#endif //SIMULATED_ANNEALING_CHARGING_STATIONS_SIMULATEDANNEALINGSERVICE_H
