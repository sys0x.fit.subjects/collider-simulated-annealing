#include <iostream>
#include "objects/PragueMap.h"
#include "services/LoadDataService.h"
#include "services/MapWriterService.h"
#include "algorithm/SimulatedAnnealingAlgorithm.h"


int main() {
    auto pragueMap = LoadDataService::generatePointOfInterestMap("../data/PointsOfInterest.csv");
    LoadDataService::loadChargingStations(pragueMap, "../data/PossChargePoint.csv");
    LoadDataService::loadBaseTemperatures(pragueMap);
    LoadDataService::removeAbsoluteZeroPotentialChargingStations(pragueMap);
    LoadDataService::loadCloseByNeighbours(pragueMap);

    MapWriterService::writeHeatMap(pragueMap,"../data/StartingHeatMapAbsoluteZero.csv");
    SimulatedAnnealingAlgorithm::anneal(pragueMap);
    MapWriterService::writeHeatMap(pragueMap,"../data/EndHeatMap-50-1000-1000-1.csv");

    return 0;
}
