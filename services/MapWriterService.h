//
// Created by malyp on 11/8/2022.
//

#ifndef SIMULATED_ANNEALING_CHARGING_STATIONS_MAPWRITERSERVICE_H
#define SIMULATED_ANNEALING_CHARGING_STATIONS_MAPWRITERSERVICE_H


#include "../objects/PragueMap.h"

class MapWriterService {
public:
    static void writeHeatMap(PragueMap & pragueMap,const std::string& newFolderName);
    static void writeChosenChargingStations(const std::vector<PossibleChargingStation *> & chosenChargingStations ,const std::string& newFolderName);

};


#endif //SIMULATED_ANNEALING_CHARGING_STATIONS_MAPWRITERSERVICE_H
