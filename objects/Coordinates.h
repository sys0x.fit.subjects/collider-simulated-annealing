//
// Created by malyp on 11/5/2022.
//

#ifndef SIMULATED_ANNEALING_CHARGING_STATIONS_COORDINATES_H
#define SIMULATED_ANNEALING_CHARGING_STATIONS_COORDINATES_H


#include <ostream>

class Coordinates {
private:
    long double coordinateX;
    long double coordinateY;
public:
    Coordinates(long double coordinateX, long double coordinateY);

    [[nodiscard]] long double getCoordinateX() const;

    [[nodiscard]] long double getCoordinateY() const;

    void setCoordinateX(long double coordinateX);

    void setCoordinateY(long double coordinateY);

    friend std::ostream &operator<<(std::ostream &os, const Coordinates &coordinates);



};


#endif //SIMULATED_ANNEALING_CHARGING_STATIONS_COORDINATES_H
