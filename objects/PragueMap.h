//
// Created by malyp on 11/5/2022.
//

#ifndef SIMULATED_ANNEALING_CHARGING_STATIONS_PRAGUEMAP_H
#define SIMULATED_ANNEALING_CHARGING_STATIONS_PRAGUEMAP_H

#include <map>
#include <vector>
#include "PointOfInterest.h"
#include "PossibleChargingStation.h"

class PragueMap {
private:
    std::map<long double,std::map<long double,PointOfInterest>> mapWithPointOfInterests;
    std::map<long double,std::map<long double,PossibleChargingStation>> mapOfPossibleChargingStations;
public:
    const std::map<long double, std::map<long double, PointOfInterest>> &getMapWithPointOfInterests() const;

    const std::map<long double, std::map<long double, PossibleChargingStation>> &getMapOfPossibleChargingStations() const;

    std::map<long double,std::map<long double,PossibleChargingStation>> &getPossibleChargingStations() ;

    void addPointOfInterest(PointOfInterest pointOfInterest);
    void addPossibleChargingPoint(PossibleChargingStation possibleChargingStation);
};


#endif //SIMULATED_ANNEALING_CHARGING_STATIONS_PRAGUEMAP_H
