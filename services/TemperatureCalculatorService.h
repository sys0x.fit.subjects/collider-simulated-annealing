//
// Created by malyp on 11/8/2022.
//

#ifndef SIMULATED_ANNEALING_CHARGING_STATIONS_TEMPERATURECALCULATORSERVICE_H
#define SIMULATED_ANNEALING_CHARGING_STATIONS_TEMPERATURECALCULATORSERVICE_H


#include <list>
#include "../objects/PointOfInterest.h"

class TemperatureCalculatorService {
private:
    static int maxPoints(InterestType interestType);
    static int minPoints(InterestType interestType);
    static double factor(InterestType interestType);
    static double linearUp(double factor,int points);
    static double arcTan(int points);
    static double exponentialDown(int distance, int maxDistance);
    static int calculateHeat(InterestType interestType, int averageDistance,int numberOfItems);

public:
    static double linearDown(int distance, int maxDistance);
    static int calculateHeat(const Coordinates &baseCoordinate, const std::list<const PointOfInterest *> &pointOfInterestsNearBy);


};


#endif //SIMULATED_ANNEALING_CHARGING_STATIONS_TEMPERATURECALCULATORSERVICE_H
