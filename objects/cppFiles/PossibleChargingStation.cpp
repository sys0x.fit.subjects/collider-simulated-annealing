//
// Created by malyp on 11/5/2022.
//

#include "../PossibleChargingStation.h"
#include "../../services/TemperatureCalculatorService.h"
#include "../../services/FinderService.h"

void PossibleChargingStation::addPointOfInterests(const std::list<const PointOfInterest *> &pointOfInterests){
    objectsUsedInBaseTemperature.insert(objectsUsedInBaseTemperature.end(),pointOfInterests.begin(),pointOfInterests.end());
}

PossibleChargingStation::PossibleChargingStation(long double coordinateX, long double coordinateY, unsigned long id)
        : Coordinates(coordinateX, coordinateY), id(id) {}

PossibleChargingStation::PossibleChargingStation(const PossibleChargingStation &possibleChargingStation)
        : Coordinates(possibleChargingStation.getCoordinateX(), possibleChargingStation.getCoordinateY()), id(possibleChargingStation.id)
        ,baseTemperature(possibleChargingStation.baseTemperature), coolingTemperature(possibleChargingStation.coolingTemperature),
        closeByChargingStations(possibleChargingStation.closeByChargingStations),coolingFactor(possibleChargingStation.coolingFactor),
        objectsUsedInBaseTemperature(possibleChargingStation.objectsUsedInBaseTemperature){}

std::ostream &operator<<(std::ostream &os, const PossibleChargingStation &station) {
    os << static_cast<const Coordinates &>(station) << " id: " << station.id;
    return os;
}

unsigned long PossibleChargingStation::getId() const {
    return id;
}

void PossibleChargingStation::addNeighbours(const std::list<PossibleChargingStation *> &neighbours) {
    closeByChargingStations.insert(closeByChargingStations.end(),neighbours.begin(),neighbours.end());

}

std::list<PossibleChargingStation *> &PossibleChargingStation::getCloseByChargingStations() {
    return closeByChargingStations;
}

const std::list<const PointOfInterest *> &PossibleChargingStation::getObjectsUsedInBaseTemperature() const {
    return objectsUsedInBaseTemperature;
}

void PossibleChargingStation::setBaseTemperature(long baseTemperature) {
    PossibleChargingStation::baseTemperature = baseTemperature;
}

long PossibleChargingStation::getBaseTemperature() const {
    return baseTemperature;
}

long PossibleChargingStation::getCoolingTemperature(PossibleChargingStation *neighbourChargingStation) const {
    auto possibleCoolingFactor = coolingFactor.find(neighbourChargingStation);
    if(possibleCoolingFactor==coolingFactor.end()){
        return 0;
    }
    int currentTemperature = baseTemperature - coolingTemperature + possibleCoolingFactor->second;
    if(currentTemperature<0){
        return 0;
    }
    return currentTemperature;
}

long PossibleChargingStation::getPossibleCoolingTemperature(PossibleChargingStation *possibleNeighbourChargingStation) const {
    auto possibleCoolingFactor = coolingFactor.find(possibleNeighbourChargingStation);
    if(possibleCoolingFactor!=coolingFactor.end()){
        return possibleCoolingFactor->second;
    }
    unsigned long distance = FinderService::calculateHypotenuse(*this,*possibleNeighbourChargingStation);
    double possibleCoolingTemperature=1000.0*TemperatureCalculatorService::linearDown(distance, 200);
    int currentTemperature = baseTemperature - coolingTemperature;
    if(currentTemperature<0){
        currentTemperature=0;
    }
    if(currentTemperature<possibleCoolingTemperature){
        return currentTemperature;
    }else{
        return possibleCoolingTemperature;
    }
}

void PossibleChargingStation::deleteNearByChargingStation(PossibleChargingStation *chargingStationToDelete) {
    auto possibleCoolingFactor = coolingFactor.find(chargingStationToDelete);
    if(possibleCoolingFactor==coolingFactor.end()) {
        return ;
    }
    int cooling= possibleCoolingFactor->second;
    coolingTemperature = coolingTemperature-cooling;
    coolingFactor.erase(possibleCoolingFactor);
}

void PossibleChargingStation::addNearByChargingStation(PossibleChargingStation *chargingStationToAdd) {
    auto possibleCoolingFactor = coolingFactor.find(chargingStationToAdd);
    if(possibleCoolingFactor!=coolingFactor.end()){
        return ;
    }
    unsigned long distance = FinderService::calculateHypotenuse(*this,*chargingStationToAdd);

    long possibleCoolingTemperature=1000.0*TemperatureCalculatorService::linearDown(distance, 200);

    coolingTemperature+=possibleCoolingTemperature;
    coolingFactor.insert(std::make_pair(chargingStationToAdd,possibleCoolingTemperature));
}

long PossibleChargingStation::getCoolingTemperature1() const {
    return coolingTemperature;
}

