//
// Created by malyp on 11/8/2022.
//

#include <map>
#include <numeric>
#include <iostream>
#include <cmath>
#include "../TemperatureCalculatorService.h"
#include "../FinderService.h"

int TemperatureCalculatorService::calculateHeat(const Coordinates &baseCoordinate,
                                                const std::list<const PointOfInterest *> &pointOfInterestsNearBy) {
//    map<interestType, list<distance>>
    std::map<InterestType, std::list<unsigned long>> distances;
    int totalHeat=0;
    for(auto pointOfInterest:pointOfInterestsNearBy){
        auto listOfDistances = distances.find(pointOfInterest->getInterestType());
        if(listOfDistances==distances.end()){
            distances.insert(std::make_pair(pointOfInterest->getInterestType(),std::list<unsigned long>()));
            listOfDistances = distances.find(pointOfInterest->getInterestType());
        }
        listOfDistances->second.push_back(FinderService::calculateHypotenuse(baseCoordinate,*pointOfInterest));
    }

    if((distances.find(CAR_PARKING)==distances.end())||
    (distances.find(CAR_PARKING)->second.size()<5)){
        return 0;
    }
    if(distances.size()>4){
        std::cout<<"wow"<<std::endl;
    }
    for(auto distance : distances){
        double distanceSum = std::accumulate(distance.second.begin(),distance.second.end(),0.0);
        double averageDistance = distanceSum/distance.second.size();
        totalHeat += calculateHeat(distance.first,averageDistance,distance.second.size());
    }


    return totalHeat;
}

int TemperatureCalculatorService::calculateHeat(InterestType interestType, int averageDistance, int numberOfItems) {
    if(maxPoints(interestType)<numberOfItems){
        numberOfItems = maxPoints(interestType);
    }
    if(numberOfItems < minPoints(interestType)){
        return 0;
    }
    double factorNum = factor(interestType);
    int heat=1000;
    switch (interestType) {
        case SHOPPING_CENTER:
        case BUILD_CHARGING_STATION:
            heat*=-1;
            heat *= linearUp(factorNum,numberOfItems);
            break;
        case ADMINISTRATION_CENTER:
        case ADMINISTRATION_BUILDING:
        case CULTURE:
        case PID:
            heat *= linearUp(factorNum,numberOfItems);
            break;
        case CAR_PARKING:
            heat *= arcTan(numberOfItems);
            break;
        default:
            std::cout << "Unknown interest type: " << interestType << ";" << std::endl;
            throw std::invalid_argument(&"Unknown interest type:"[interestType]);
    }
    if(interestType==SHOPPING_CENTER){
        heat *= exponentialDown(averageDistance,500);
    }else{
        heat *= linearDown(averageDistance,FinderService::getWalkingDistance(interestType));
    }
    return heat;
}

int TemperatureCalculatorService::maxPoints(InterestType interestType) {
    switch (interestType) {
        case SHOPPING_CENTER:
            return 5;
        case ADMINISTRATION_CENTER:
            return 20;
        case ADMINISTRATION_BUILDING:
            return 30;
        case CULTURE:
            return 40;
        case BUILD_CHARGING_STATION:
            return 1;
        case PID:
            return 14;
        case CAR_PARKING:
            return 1000;
        default:
            std::cout << "Unknown interest type: " << interestType << ";" << std::endl;
            throw std::invalid_argument(&"Unknown interest type:"[interestType]);
    }
}

int TemperatureCalculatorService::minPoints(InterestType interestType) {
    switch (interestType) {
        case SHOPPING_CENTER:
            return 1;
        case ADMINISTRATION_CENTER:
            return 1;
        case ADMINISTRATION_BUILDING:
            return 1;
        case CULTURE:
            return 1;
        case BUILD_CHARGING_STATION:
            return 1;
        case PID:
            return 1;
        case CAR_PARKING:
            return 5;
        default:
            std::cout << "Unknown interest type: " << interestType << ";" << std::endl;
            throw std::invalid_argument(&"Unknown interest type:"[interestType]);
    }
}

double TemperatureCalculatorService::factor(InterestType interestType) {
    switch (interestType) {
        case SHOPPING_CENTER:
            return 0.1;
        case ADMINISTRATION_CENTER:
            return 0.032;
        case ADMINISTRATION_BUILDING:
            return 0.048;
        case CULTURE:
            return 0.024;
        case BUILD_CHARGING_STATION:
            return 1;
        case PID:
            return 0.0686;
        case CAR_PARKING:
            return 0.1;
        default:
            std::cout << "Unknown interest type: " << interestType << ";" << std::endl;
            throw std::invalid_argument(&"Unknown interest type:"[interestType]);
    }

}

double TemperatureCalculatorService::linearUp(double factor, int points) {
    return factor*points;
}

double TemperatureCalculatorService::linearDown(int distance, int maxDistance) {
    if(distance>maxDistance){
        return  0 ;
    }
    return 1-(double)distance/maxDistance;
}

double TemperatureCalculatorService::arcTan(int points) {
    double result = (atan(points-10)/M_PI)+(0.468274482569446);
    return result;
}

double TemperatureCalculatorService::exponentialDown(int distance, int maxDistance) {
    if(distance>maxDistance){
        return 0;
    }
    return exp(-1*distance*0.01);
}
