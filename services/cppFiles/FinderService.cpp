//
// Created by malyp on 11/5/2022.
//

#include <iostream>
#include <cmath>
#include "../FinderService.h"

long FinderService::getWalkingDistance(InterestType interestType) {
    switch (interestType) {
        case SHOPPING_CENTER:
            return 1000;
        case ADMINISTRATION_CENTER:
            return 500;
        case ADMINISTRATION_BUILDING:
            return 300;
        case CULTURE:
            return 1000;
        case BUILD_CHARGING_STATION:
            return 100;
        case PID:
            return 400;
        case CAR_PARKING:
            return 50;
        default:
            std::cout << "Unknown interest type: " << interestType << ";" << std::endl;
            throw std::invalid_argument(&"Unknown interest type:"[interestType]);
    }
}

std::list<InterestType> FinderService::getInterestTypesToFind() {
    auto toFind= std::list<InterestType>();
    toFind.push_back(SHOPPING_CENTER);
    toFind.push_back(ADMINISTRATION_CENTER);
    toFind.push_back(ADMINISTRATION_BUILDING);
    toFind.push_back(CULTURE);
    toFind.push_back(BUILD_CHARGING_STATION);
    toFind.push_back(PID);
    toFind.push_back(CAR_PARKING);
    return toFind;
}

std::list<const PointOfInterest *> FinderService::getInterestPointsByType(const PragueMap &pragueMap,
                                                                  const Coordinates &searchArea,
                                                                  const InterestType &interestType) {
    auto walkingDistance = getWalkingDistance(interestType);
    auto lowerBoundX = pragueMap.getMapWithPointOfInterests().lower_bound(searchArea.getCoordinateX()-walkingDistance/2);
    auto upperBoundX = pragueMap.getMapWithPointOfInterests().upper_bound(searchArea.getCoordinateX()+walkingDistance/2);
    auto interestingPointsCircle=std::list<const PointOfInterest * >();

    for(auto row=lowerBoundX;row!=upperBoundX;row++){
        auto lowerBoundY = row->second.lower_bound(searchArea.getCoordinateY()-walkingDistance/2);
        auto upperBoundY = row->second.upper_bound(searchArea.getCoordinateY()+walkingDistance/2);
        for(auto point=lowerBoundY;point!=upperBoundY;point++){
            unsigned long distance = calculateHypotenuse(point->second,searchArea);
            if(distance<=walkingDistance&&point->second.getInterestType()==interestType){
                interestingPointsCircle.push_back(&(point->second));
            }
        }
    }
    return interestingPointsCircle;

}

unsigned long FinderService::calculateHypotenuse(const Coordinates &one, const Coordinates &two) {
    long double deltaX=std::pow(one.getCoordinateX()-two.getCoordinateX(),2);
    long double deltaY=std::pow(one.getCoordinateY()-two.getCoordinateY(),2);
    long double distance = std::sqrt(deltaX+deltaY);
    return distance;
}

std::list<PossibleChargingStation *>
FinderService::getAllCloseByChargingStations(PragueMap &pragueMap, const Coordinates &searchArea,
                                             const int &walkingDistance,bool useCircle) {
    auto lowerBoundX = pragueMap.getPossibleChargingStations().lower_bound(searchArea.getCoordinateX()-walkingDistance/2);
    auto upperBoundX = pragueMap.getPossibleChargingStations().upper_bound(searchArea.getCoordinateX()+walkingDistance/2);
    auto neighbours=std::list<PossibleChargingStation * >();

    for(auto row=lowerBoundX;row!=upperBoundX;row++) {
        auto lowerBoundY = row->second.lower_bound(searchArea.getCoordinateY() - walkingDistance / 2);
        auto upperBoundY = row->second.upper_bound(searchArea.getCoordinateY() + walkingDistance / 2);
        for(auto point=lowerBoundY;point!=upperBoundY;point++){
            if(useCircle) {
                unsigned long distance = calculateHypotenuse(point->second, searchArea);
                if (distance <= walkingDistance && searchArea.getCoordinateX() != point->second.getCoordinateX() &&
                    searchArea.getCoordinateY() != point->second.getCoordinateY()) {
                    neighbours.push_back(&(point->second));
                }
            }else{
                if (searchArea.getCoordinateX() != point->second.getCoordinateX() &&
                    searchArea.getCoordinateY() != point->second.getCoordinateY()) {
                    neighbours.push_back(&(point->second));
                }
            }
        }
    }

    return neighbours;
}


