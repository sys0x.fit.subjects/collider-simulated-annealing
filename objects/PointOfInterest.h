//
// Created by malyp on 11/5/2022.
//

#ifndef SIMULATED_ANNEALING_CHARGING_STATIONS_POINTOFINTEREST_H
#define SIMULATED_ANNEALING_CHARGING_STATIONS_POINTOFINTEREST_H

#include <ostream>
#include "Coordinates.h"

enum InterestType {
    SHOPPING_CENTER,
    ADMINISTRATION_CENTER,
    ADMINISTRATION_BUILDING,
    CULTURE,
    BUILD_CHARGING_STATION,
    PID,
    CAR_PARKING
};

class PointOfInterest : public Coordinates{
private:
    unsigned long id;
    InterestType interestType;
    unsigned long noise;
public:
    PointOfInterest(long double coordinateX, long double coordinateY, unsigned long id,unsigned long noise, InterestType interestType);

    PointOfInterest(PointOfInterest const &pointOfInterest);

    InterestType getInterestType() const;

    friend std::ostream &operator<<(std::ostream &os, const PointOfInterest &interest);
};


#endif //SIMULATED_ANNEALING_CHARGING_STATIONS_POINTOFINTEREST_H
