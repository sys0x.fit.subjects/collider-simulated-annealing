//
// Created by malyp on 11/6/2022.
//

#ifndef SIMULATED_ANNEALING_CHARGING_STATIONS_SIMULATEDANNEALINGALGORITHM_H
#define SIMULATED_ANNEALING_CHARGING_STATIONS_SIMULATEDANNEALINGALGORITHM_H


#include "../objects/PragueMap.h"

class SimulatedAnnealingAlgorithm {
private:
    static std::vector<PossibleChargingStation *> pickStartingChargingStations(PragueMap & pragueMap, std::vector<PossibleChargingStation *> &pickedChargingStations);
public:
    static void anneal(PragueMap & pragueMap);


//    poc teplota nenastavovat udelat spetny chod..
// koncovou teplotu nenastavovat ale sledovat vyvoj
// co nastavit: rychlost ochlazovani
};


#endif //SIMULATED_ANNEALING_CHARGING_STATIONS_SIMULATEDANNEALINGALGORITHM_H
