//
// Created by malyp on 11/5/2022.
//

#include <random>
#include "../SimulatedAnnealingService.h"
#include "../FinderService.h"

int SimulatedAnnealingService::calculateCurrentTemperatureDelta(PossibleChargingStation &possibleChargingStation) {
    int temperatureDelta = 0;
    for(auto & chargingPoint: possibleChargingStation.getCloseByChargingStations()){
        temperatureDelta+=chargingPoint->getCoolingTemperature(&possibleChargingStation);
    }
    return temperatureDelta;
}

int
SimulatedAnnealingService::calculateNewTemperatureDelta(PossibleChargingStation &possibleChargingStation) {
    int temperatureDelta = 0;
    for(auto & chargingPoint: possibleChargingStation.getCloseByChargingStations()){
        temperatureDelta+=chargingPoint->getPossibleCoolingTemperature(&possibleChargingStation);
    }
    return temperatureDelta;
}

std::vector<PossibleChargingStation *> SimulatedAnnealingService::getNeighbours(PragueMap &pragueMap,
                                                                              const Coordinates &startingCoordinates,
                                                                              const int &currentTemperature) {
    auto neighbours= FinderService::getAllCloseByChargingStations(pragueMap,startingCoordinates,currentTemperature,false);
    std::vector<PossibleChargingStation *> result(neighbours.begin(), neighbours.end());
    return result;

}

int SimulatedAnnealingService::calculateSwitchTemperatureDelta(PossibleChargingStation &currentChargingStation,
                                                               PossibleChargingStation &proposedChargingStation) {
    int oldTemperatureDelta = calculateCurrentTemperatureDelta(currentChargingStation);
    int newTemperatureDelta = calculateNewTemperatureDelta(proposedChargingStation);

    return newTemperatureDelta-oldTemperatureDelta;
}

bool SimulatedAnnealingService::shouldISwitch(PossibleChargingStation & currentChargingStation, PossibleChargingStation &possibleChargingStation,
                                              const int &currentTemperature) {
    std::knuth_b engine;

    int switchDelta=calculateSwitchTemperatureDelta(currentChargingStation, possibleChargingStation);
    if(switchDelta>0){
        return true;
    }else{
        double probability = std::exp(switchDelta/currentTemperature);
        std::bernoulli_distribution distribution(probability);
        return distribution(engine);
    }
}

void SimulatedAnnealingService::switchChargingStations(PossibleChargingStation &oldChargingStation,
                                                       PossibleChargingStation &newStation) {
    oldChargingStation.deleteNearByChargingStation(&oldChargingStation);
    for(auto closeByChargingStation : oldChargingStation.getCloseByChargingStations()){
        closeByChargingStation->deleteNearByChargingStation(&oldChargingStation);
    }
    newStation.addNearByChargingStation(&newStation);
    for(auto closeByChargingStation : newStation.getCloseByChargingStations()){
        closeByChargingStation->addNearByChargingStation(&newStation);
    }

}

void SimulatedAnnealingService::addChargingStation(PossibleChargingStation &newStation) {
    newStation.addNearByChargingStation(&newStation);
    for(auto closeByChargingStation : newStation.getCloseByChargingStations()){
        closeByChargingStation->addNearByChargingStation(&newStation);
    }
}
