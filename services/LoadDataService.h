//
// Created by malyp on 11/5/2022.
//

#ifndef SIMULATED_ANNEALING_CHARGING_STATIONS_LOADDATASERVICE_H
#define SIMULATED_ANNEALING_CHARGING_STATIONS_LOADDATASERVICE_H


#include "../objects/PragueMap.h"
#include <vector>

class LoadDataService {
private:
    static std::vector<std::vector<std::string> > parseCsv(std::string csvFileLocation);
    static InterestType parseInterestType(std::string interestTypeString);

public:
    static PragueMap generatePointOfInterestMap(std::string csvFileLocation);
    static void loadChargingStations(PragueMap &pragueMap, const std::string & csvFileLocation);
    static void loadCloseByNeighbours(PragueMap &pragueMap);
    static void loadBaseTemperatures(PragueMap &pragueMap);
    static void removeAbsoluteZeroPotentialChargingStations(PragueMap &pragueMap);

};


#endif //SIMULATED_ANNEALING_CHARGING_STATIONS_LOADDATASERVICE_H
