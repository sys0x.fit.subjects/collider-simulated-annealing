//
// Created by malyp on 11/8/2022.
//
#include <iostream>
#include <fstream>
#include <iomanip>
#include "../MapWriterService.h"

void MapWriterService::writeHeatMap(PragueMap &pragueMap, const std::string& newFolderName) {
    // Create and open a text file
    std::ofstream newFile(newFolderName);

    std::cout<<"Write to file heat map, file name:"<<newFolderName<<std::endl;
    newFile<<"OID_;X;Y;TEMPERATURE"<<std::endl;
    int i=0;
    int highestTemperature = 0;
    int allTemperature = 0;
    for (auto &possibleChargingStationRow : pragueMap.getPossibleChargingStations()){
        for(auto & possibleChargingStationPoint : possibleChargingStationRow.second){
            i++;
            int temperature = possibleChargingStationPoint.second.getBaseTemperature()-possibleChargingStationPoint.second.getCoolingTemperature1();
            if(temperature<0){
                temperature=0;
            }
            newFile<< std::fixed << std::setprecision(15)<<possibleChargingStationPoint.second.getId()<<";";
            newFile<<possibleChargingStationPoint.second.getCoordinateX()<<";"<<possibleChargingStationPoint.second.getCoordinateY()<<";";

            newFile<<temperature<<std::endl;
            allTemperature+=temperature;
            if(highestTemperature<temperature){
                highestTemperature=temperature;
            }

        }
    }
    std::cout<<"Highest temperature:"<<highestTemperature<<std::endl;
    std::cout<<"All temperature:"<<allTemperature<<std::endl;
    // Close the file
    newFile.close();
}

void MapWriterService::writeChosenChargingStations(const std::vector<PossibleChargingStation *> &chosenChargingStations,
                                                   const std::string &newFolderName) {
    std::ofstream newFile(newFolderName);

    std::cout << "Write to file heat map, file name:" << newFolderName << std::endl;
    newFile << "OID_;X;Y;TEMPERATURE" << std::endl;
    for (auto &possibleChargingStationPoint: chosenChargingStations) {

        newFile<< std::fixed << std::setprecision(15)<<possibleChargingStationPoint->getId()<<";";
        newFile<<possibleChargingStationPoint->getCoordinateX()<<";"<<possibleChargingStationPoint->getCoordinateY()<<";";
        int temperature = possibleChargingStationPoint->getBaseTemperature()-possibleChargingStationPoint->getCoolingTemperature1();
        if(temperature<0){
            temperature=0;
        }
        newFile<<temperature<<std::endl;
    }
    // Close the file
    newFile.close();
}
